/**
 * Adobe ExtendScript for Illustrator CS5+
 * ai.jsx (c)MaratShagiev m_js@bk.ru 24.02.2017
 *
 * embedLinks
 *
 * 0.0.2
 * */

//@target illustrator

embedLinks('ambedLinks', '0.0.1');

function embedLinks(esName, esVers) {

  var store = new Store(esName, esVers);

  /**
   * INTERFACE
   * */
  var win       = new Window('dialog', 'Embed links in multiple ai ' + esVers),
      folderPan = win.add('panel', undefined, 'Folders'),
      inGr      = folderPan.add('group');

  win.outGr = folderPan.add('group');

  win.inFld  = inGr.add('edittext', [0, 0, 300, 25]);
  var inBtn  = inGr.add('button', [0, 0, 100, 25], 'Input from');
  win.outFld = win.outGr.add('edittext', [0, 0, 300, 25]);
  var outBtn = win.outGr.add('button', [0, 0, 100, 25], 'Output to');

  win.changeSavePathChk = win.add('checkbox', undefined, 'Change save path');

  var btnGr       = win.add('group'),
      progressTxt = btnGr.add('statictext', [0, 0, 100, 40], 'Processed files: ');

  win.progrValTxt = btnGr.add('statictext', [0, 0, 50, 40], '0');

  var kostiylTxt = btnGr.add('statictext', [0, 0, 50, 20]),
      okBtn      = btnGr.add('button', undefined, 'OK'),
      cancelBtn  = btnGr.add('button', undefined, 'Cancel');

  win.alignChildren = 'left';
  btnGr.alignment   = 'center';

  store.setFaceValues(win);

  win.changeSavePathChk.onClick = function() {
    if (this.value) {
      win.outGr.enabled = true;
    } else {
      win.outGr.enabled = false;
    }
  }

  inBtn.onClick = function() {
    enterFolderPath(win.inFld);
  }

  outBtn.onClick = function() {
    enterFolderPath(win.outFld)
  }

  okBtn.onClick = function() {
    store.setIniValues(win);

    var opts = store.getFaceValues(win);
    try {
      core(opts, win);
      win.close();
    } catch (e) {
      alert('line ' + e.line + '\n' + e.message);
    }
  }

  win.show();

  /**
   * INTERFACE LIBRARY
   * */
  function enterFolderPath(dialogFldElem) {
    var savePth = Folder.selectDialog();
    if (savePth !== null) {
      dialogFldElem.text = savePth;
    }
  }

  /**
   * MAIN ACTION
   * */
  function core(opts, win) {
    var isChngFnt              = parseChk(opts.changeSavePathChk),
        strokes                = parseStrokes(opts.strokeFld),
        inputPath              = opts.inFld,
        outPath                = opts.outFld,
        inputFolder            = new Folder(inputPath),
        outputFolder           = new Folder(outPath),
        userInteractLevelStore = app.userInteractionLevel,
        changeSavePathChk      = opts.changeSavePathChk;

    app.userInteractionLevel = UserInteractionLevel.DONTDISPLAYALERTS;

    if (!inputFolder.exists) {
      throw new Error('Incorrect input folder');
    }
    if (!outputFolder.exists && changeSavePathChk) {
      throw new Error('Incorrect output folder');
    }

    var inputFiles  = inputFolder.getFiles("*.ai"),
        pdfSaveOpts = new PDFSaveOptions();

    for (var i = 0; i < inputFiles.length; i++) {
      var aiFile = inputFiles[i];

      open(aiFile);
      embedRasters();

      if (changeSavePathChk) {
        var aiSaveOpts = new IllustratorSaveOptions();
        var aiNewFile  = new File(outPath);
        activeDocument.saveAs(aiNewFile, aiSaveOpts);
        activeDocument.close(SaveOptions.DONOTSAVECHANGES);
      } else {
        activeDocument.close(SaveOptions.SAVECHANGES);
      }

      var progrVal         = +win.progrValTxt.text;
      win.progrValTxt.text = ++progrVal;
      win.update();
    }

    app.userInteractionLevel = userInteractLevelStore;

    /**
     * _CORE_LIBRARY
     * */

    function parseChk(val) {
      if (typeof val === 'boolean') return val;
      var resVal = false;
      if (val == 'true') resVal = true;
      return resVal;
    }

    function parseStrokes(strokes) {
      if (!strokes) return [];
      var s = strokes;

      s     = s.replace(/^[^\d]+/, ''); // clean start ot string
      s     = s.replace(/,/g, '.'); // all commas to floating points
      s     = s.replace(/(\.)+/g, '.'); // multiple floating points to one point
      s     = s.replace(/[,.]+([^.\d])+/g, '$1'); //  no floating point context after point
      s     = s.replace(/([^.\d])+[.,]+/g, '$1'); //  no floating point context before point
      var a = s.split(/[^,.\d]+/g);
      var b = [];
      for (var i = 0; i < a.length; i += 2) {
        b.push([+a[i], +a[i + 1]]);
      }
      return b;
    }

    function embedRasters() {
      var d          = activeDocument;
      var rasters    = d.placedItems;
      var rastersLen = rasters.length;
      for (var i = rastersLen - 1; i >= 0; i--) {
        var raster = rasters[i];
        raster.embed();
      }
    }
  }

  /**
   * COMMON LIBRARY
   * */
  /**
   * operating with varlues of the panel
   * and save settings on ini-file
   * path to save is relatively:
   * * userData/LocalStorage/duplicateItems/duplicateItems.ini
   *
   * @method{setFaceValues} load the values to interface from ini-file or from defaults object
   * @method{getFaceValues}
   * @method{setIniValues}
   * @constructor
   *
   * */
  function Store(esName, esVers) {
    /**
     * make the default values for the interface
     *
     * @constructor
     * */
    function Defaults() {
      this.inFld  = '';
      this.outFld = '';

      this.changeSavePathChk = 'false';
    }

    /**
     * @public
     * @param {Window} win - ExtendScript UI class object
     * */
    this.setFaceValues = function(win) {
      var values = _getIniValues();

      for (var key in values) {
        if (key == 'saveOptsChk' || key == 'changeSavePathChk') {
          var boolValue = false;
          if (values[key] == 'true') {
            boolValue = true;
          }
          win[key].value = boolValue;
          continue;
        }
        win[key].text = values[key];
      }
      if (win.changeSavePathChk.value) {
        win.outGr.enabled = true;
      } else {
        win.outGr.enabled = false;
      }
    }

    /**
     * @public
     * @param {Window} win - ExtendScript UI class object
     * */
    this.setIniValues = function(win) {
      var iniFile  = _getIniFile();
      var faceOpts = this.getFaceValues(win);
      var defaults = new Defaults();

      _clearIni(iniFile);

      iniFile.open('w');

      for (var key in defaults) {
        var iniKey = key;
        var iniVal = faceOpts[key];

        if (iniVal === '') {
          iniVal = defaults[key];
        }
        iniFile.writeln(iniKey);
        iniFile.writeln(iniVal);
      }
      iniFile.close();
    }

    /**
     * @public
     * @param {Window} win - ExtendScript UI class object
     * @return {Object} opts - values of interface
     * */
    this.getFaceValues = function(win) {
      var opts = new Defaults();
      for (var key in opts) {
        if (key == 'saveOptsChk' || key == 'changeSavePathChk') {
          opts[key] = win[key].value;
          continue;
        }
        opts[key] = win[key].text;
      }
      return opts;
    }

    /**
     * get or create ini-file where storing the interface values
     *
     * @private
     * */
    function _getIniFile() {
      var storageFolder = new Folder(Folder.userData + '/LocalStorage/' + esName + '/');
      if (!storageFolder.exists) {
        storageFolder.create();
      }
      var iniFile = new File(storageFolder.fullName + '/' + esName + '_v' + esVers + '.ini');
      if (!iniFile.exists) {
        iniFile.open('w');
        iniFile.close();
      }
      return iniFile;
    }

    /**
     * clear ini-file through deletion and recreation
     *
     * @private
     * @param {File} f - Object of the ExtendScript File class
     * */
    function _clearIni(f) {
      f.remove();
      f.open('w');
      f.close();
    }

    /**
     * read {key: value} from ini-file
     * odd lines are keys, even lines are values
     *
     * @private
     * @return {Object} opts - values that paste to interface
     * */
    function _getIniValues() {
      var opts    = new Defaults();
      var iniFile = _getIniFile();

      iniFile.open('r');

      for (var value in opts) {
        var key = iniFile.readln();
        var val = iniFile.readln();
        if (key === '') continue;
        if (val === '') continue;
        opts[key] = val;
      }
      iniFile.close();
      return opts;
    }

  }
}